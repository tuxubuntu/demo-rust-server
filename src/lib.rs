extern crate containerizer;
extern crate message;
extern crate rand;

mod utils;

pub mod rpc;
pub mod tcp;
pub mod unix;
pub mod router;

pub use rpc::sender::Sender;

use std::sync::mpsc;

type Result<T> = std::result::Result<T, String>;
type Code = u8;
type Data = Vec<u8>;
type Response = Result<Data>;
type Message = (u8, u32, Data);
type Handler = Box<FnMut(usize, Data, Sender) -> Response + Send>;

trait Rpc<C: Client> where Self: Send {
	fn listen<S: 'static>(&mut self, S) where S: Server<Client=C>;
	fn connect(&mut self, C);
	fn on_connect(&mut self) -> mpsc::Receiver<usize>;
	fn sender(&self) -> Sender;
}

pub trait Server where Self: Send {
	type Client;
	fn start(self) -> mpsc::Receiver<Self::Client>;
}

pub trait Client where Self: Clone+Send {
	fn read(&mut self) -> Result<Message>;
	fn write(&mut self, Message) -> Result<usize>;
}

pub trait Router where Self: Send {
	fn handle(&mut self, u8, Handler);
	fn handler(&mut self, u8) -> Option<&mut Handler>;
}


#[cfg(test)]
mod tests;
