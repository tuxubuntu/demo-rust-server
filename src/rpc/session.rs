use std::collections::HashMap;
use super::super::Response;
use std::sync::mpsc;

pub struct Sessions {
	list: HashMap<(usize, u32), mpsc::Sender<Response>>
}

type Session = mpsc::Receiver<Response>;

impl Sessions {
	pub fn new() -> Sessions {
		Sessions {
			list: HashMap::new()
		}
	}
	fn gen_mid(&mut self, cid: usize) -> u32 {
		use rand;
		loop {
			let mid = rand::random::<u32>();
			if !self.list.contains_key(&(cid, mid)) {
				return mid
			}
		}
	}
	pub fn reg(&mut self, cid: usize) -> (u32, Session) {
		let mid = self.gen_mid(cid);
		let id = (cid, mid);
		let (sender, receiver) = mpsc::channel();
		self.list.insert(id, sender);
		(mid, receiver)
	}
	pub fn has(&mut self, cid: usize, mid: u32) -> bool {
		let id = (cid, mid);
		self.list.contains_key(&id)
	}
	pub fn resolve(&mut self, cid: usize, mid: u32, data: Response) -> bool {
		let id = (cid, mid);
		match self.list.remove_entry(&id) {
			Some((_, res)) => {
				res.send(data).unwrap();
				true
			},
			None => false,
		}
	}
}


		// let session = Session::from(receiver);
		// let session = session.then(|res| {
		// 	match res {
		// 		Ok(res) => {
		// 			res
		// 		},
		// 		Err(err) => {
		// 			Err(String::from(format!("{:?}", err)))
		// 		},
		// 	}
		// });