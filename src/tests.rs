

#[test]
fn tcp() {
	use Rpc;
	use Router;
	use rpc::SimpleRpc;
	use router::SimpleRouter;
	use tcp::server::SimpleTcpServer;
	use tcp::client::SimpleTcpClient;
	use std::thread;
	use std::time;
	thread::spawn(move || {
		thread::sleep(time::Duration::from_millis(100));
		let mut host_b = SimpleRpc::from(SimpleRouter::new());
		let mut sender = host_b.sender();
		let recv = host_b.on_connect();
		thread::spawn(move || {
			recv.recv().unwrap();
			assert_eq!(Ok(vec![1, 2, 3, 0]), sender.send(0, 42, vec![1, 2, 3]));
			assert_eq!(Err(String::from("Endpoint not found.")), sender.send(0, 22, vec![1, 2, 3]));
		});
		host_b.connect(SimpleTcpClient::from("0.0.0.0:9999"));
	});
	let mut router = SimpleRouter::new();
	router.handle(42, Box::new(move |cid, mut data, _| {
		data.push(cid as u8);
		Ok(data)
	}));
	let mut host_a = SimpleRpc::from(router);
	host_a.listen(SimpleTcpServer::from("0.0.0.0:9999"));
}

#[test]
fn unix() {
	use Rpc;
	use Router;
	use rpc::SimpleRpc;
	use router::SimpleRouter;
	use unix::server::SimpleUnixServer;
	use unix::client::SimpleUnixClient;
	use std::thread;
	use std::time;
	thread::spawn(move || {
		thread::sleep(time::Duration::from_millis(100));
		let mut host_b = SimpleRpc::from(SimpleRouter::new());
		let mut sender = host_b.sender();
		let recv = host_b.on_connect();
		thread::spawn(move || {
			recv.recv().unwrap();
			assert_eq!(Ok(vec![1, 2, 3, 0]), sender.send(0, 42, vec![1, 2, 3]));
			assert_eq!(Err(String::from("Endpoint not found.")), sender.send(0, 22, vec![1, 2, 3]));
		});
		host_b.connect(SimpleUnixClient::from("/tmp/gdi.sock"));
	});
	let mut router = SimpleRouter::new();
	router.handle(42, Box::new(move |cid, mut data, _| {
		data.push(cid as u8);
		Ok(data)
	}));
	let mut host_a = SimpleRpc::from(router);
	host_a.listen(SimpleUnixServer::from("/tmp/gdi.sock"));
}

#[test]
fn exit() {
	use std::thread;
	use std::time;
	use std::process;
	thread::sleep(time::Duration::from_millis(500));
	process::exit(0);
}
