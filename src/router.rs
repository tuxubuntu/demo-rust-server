use std::collections::HashMap;

use super::{Code, Router, Handler};


pub struct SimpleRouter {
	handlers: HashMap<u8, Handler>
}

impl SimpleRouter {
	pub fn new() -> SimpleRouter {
		SimpleRouter {
			handlers: HashMap::new()
		}
	}
}

impl Router for SimpleRouter {
	fn handle(&mut self, code: Code, handler: Handler) {
		self.handlers.insert(code, handler);
	}
	fn handler(&mut self, code: Code) -> Option<&mut Handler> {
		self.handlers.get_mut(&code)
	}
}

