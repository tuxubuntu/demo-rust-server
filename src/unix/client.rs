
use super::super::Message;
use super::super::Client;
use super::super::Result;
use super::super::utils::{encode, decode};


use std::io::Write;
use std::io::Read;

use std::os::unix::net::UnixStream;


pub struct SimpleUnixClient {
	stream: UnixStream
}

impl Clone for SimpleUnixClient {
	fn clone(&self) -> SimpleUnixClient {
		let stream = self.stream.try_clone().unwrap();
		SimpleUnixClient::from(stream)
	}
}


impl<'a> From<&'a str> for SimpleUnixClient {
	fn from(addr: &'a str) -> SimpleUnixClient {
		let stream = UnixStream::connect(addr).unwrap();
		SimpleUnixClient::from(stream)
	}
}

impl From<UnixStream> for SimpleUnixClient {
	fn from(stream: UnixStream) -> SimpleUnixClient {
		SimpleUnixClient {
			stream: stream,
		}
	}
}

impl Client for SimpleUnixClient {
	fn read(&mut self) -> Result<Message> {
		let mut state = State::Init;
		loop {
			state = read(&mut self.stream, state);
			match state {
				State::Data(data) => {
					let msg = decode(data);
					break Ok(msg)
				},
				State::Err(err) => {
					break Err(err)
				},
				_ => (),
			}
		}
	}
	fn write(&mut self, msg: Message) -> Result<usize> {
		let raw = encode(msg).unwrap();
		match self.stream.write(&raw) {
			Ok(len) => Ok(len),
			Err(err) => Err(String::from(format!("{}", err))),
		}
	}
}

use containerizer::{
	Container,
	raw::{ RawLen32, RawLen64 }
};

#[derive(Debug)]
pub enum State {
	Init,
	Code(u8),
	Len(usize),
	Data(Vec<u8>),
	Err(String),
}

pub fn read(stream: &mut UnixStream, state: State) -> State {
	match state {
		State::Init => {
			let mut buf = vec![0; 1];
			match read_buf(stream, buf) {
				Ok(buf) => State::Code(buf[0]),
				Err(err) => State::Err(err),
			}
		},
		State::Code(code) => {
			if code == RawLen32::code() {
				let mut buf = vec![0; 4];
				match read_buf(stream, buf) {
					Ok(buf) => State::Len(RawLen32::len(&buf)),
					Err(err) => State::Err(err),
				}
			} else if code == RawLen64::code() {
				let mut buf = vec![0; 8];
				match read_buf(stream, buf) {
					Ok(buf) => State::Len(RawLen64::len(&buf)),
					Err(err) => State::Err(err),
				}
			} else {
				State::Err(String::from("Wrong Code"))
			}
		},
		State::Len(len) => {
			let mut buf = vec![0; len];
			match read_buf(stream, buf) {
				Ok(buf) => State::Data(buf),
				Err(err) => State::Err(err),
			}
		},
		State::Data(data) => State::Data(data),
		State::Err(err) => panic!(err),
	}
}

fn read_buf(stream: &mut UnixStream, mut buf: Vec<u8>) -> Result<Vec<u8>> {
	match stream.read(&mut buf) {
		Ok(0) => Err(String::from("Connection was closed")),
		Ok(_) => Ok(buf),
		Err(err) =>Err(String::from(format!("{}", err))),
	}
}

